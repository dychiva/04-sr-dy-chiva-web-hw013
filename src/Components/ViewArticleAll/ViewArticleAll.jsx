import React, { Component } from "react";
import axios from "axios";
import { Table, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default class ViewArticleAll extends Component {
  constructor() {
    super();
    this.state = {
      articles: [],
      id: "",
    };
    // this.onView = this.onView.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  componentWillMount() {
    this.handleGetList();
  }

  //Get all data 
  handleGetList() {
    axios
      .get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
      .then((respone) => {
        this.setState({
          articles: respone.data.DATA,
        });
      });
  }

  //Convert Date
   convertDate = (created_date) =>{
        let parameterDate = created_date;
        let year = parameterDate.substring(0,4);
        let month = parameterDate.substring(4,6);
        let day = parameterDate.substring(6,8);
        let formalDate = day + "/" + month + "/" + year;
        return formalDate;
    }

    //Delete Record
  onDelete = (id) => {
    axios
      .delete("http://110.74.194.124:15011/v1/api/articles/" + id)
      .then((res) => {
        this.handleGetList();
        alert(res.data.MESSAGE);
        console.log(res.data.DATA);
      });
  };

  render() {
    return (
      <div className="container">
        <div
          style={{
            textAlign: "center",
            marginBottom: "20px",
            marginTop: "20px",
          }}
        >
          <h1>Article Management</h1>
          <Link to="/add">
            <Button variant="dark">Add New Article</Button>
          </Link>
        </div>

        <Table striped bordered >
          <thead>
            <tr>
              <th>#</th>
              <th>TITLE</th>
              <th>DESCRIPTION</th>
              <th>CREATE DATE</th>
              <th>IMAGE</th>
              <th>ACTION</th>
            </tr>
          </thead>
          <tbody>
            {this.state.articles.map((article, index) => (
              <tr key={index}>
                <td>{article.ID}</td>
                <td>{article.TITLE}</td>
                <td>{article.DESCRIPTION}</td>
                <td>{this.convertDate(article.CREATED_DATE)}</td>
                <td>
                  <img width="100" src={article.IMAGE} alt="Avata" />
                </td>
                <td className="align-middle" >
                  <div className="d-flex justify-content-around">
                    <Link to={`/view/${article.ID}`}>
                      <button className="d-inline-block btn btn-primary">
                        View
                      </button>
                    </Link>
                    &nbsp;
                    <Link to={`/add/?isUpdate=true&&id=${article.ID}`}>
                      <button className="d-inline-block btn btn-warning">
                        Edit
                      </button>
                    </Link>
                    &nbsp;
                    <button
                      className="d-inline-block btn btn-danger"
                      onClick={() => this.onDelete(article.ID)}
                    >
                      Delete
                    </button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div> 
    );
  }
}
