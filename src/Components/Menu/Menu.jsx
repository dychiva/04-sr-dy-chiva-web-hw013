
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import React, { Component } from 'react'

export default class Menu extends Component {
  render() {
    return (
      <div>
         <Navbar bg="light" variant="light">
        <div className="container">
          <Navbar.Brand >AMS</Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
          </Nav>
          <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button variant="outline-primary">Search</Button>
          </Form>
        </div>
      </Navbar>
      </div>
    )
  }
}

