import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

export default class ViewDetail extends Component {
  constructor() {
    super();
    this.state = {
      details: [],
    };
  }

  componentWillMount() {
    this.getDetail();
  }

  getDetail() {
    var id = this.props.match.params.id;
    axios
      .get("http://110.74.194.124:15011/v1/api/articles/" + id)
      .then((res) => {
        this.setState({
          details: res.data.DATA,
        });
      });
  }

  render() {
    return (
      <div className="container">
         
        <div
          className="row"
          style={{
              width:"70%",
              margin: "0 auto",
            textAlign: "center",
            marginBottom: "20px",
            marginTop: "20px",
          }}
        >
           
          <div className="col-sm-4">
           
            <img
              width="250"
              src={this.state.details.IMAGE}
              alt={this.state.details.IMAGE}
            />
          </div>

          <div className="col-sm-8" style={{marginRight:"0"}}>
            <h6> {this.state.details.TITLE}</h6>
            <p>{this.state.details.DESCRIPTION}</p>
            <Link to="/">
                <p>See more...</p>
            </Link>
          
          </div>
        </div>
      </div>
    );
  }
}
