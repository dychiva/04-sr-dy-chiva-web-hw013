import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import Axios from "axios";
import queryString from "query-string";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPen } from '@fortawesome/free-solid-svg-icons'
export default class AddArticle extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      title: "",
      description: "",
      isUpdate: false,
      btnName: "submit",
      header: "Add Article",
      spcId: "",
      titleValidate: "",
      descValidate: "",
      selectedFile:
        "https://iwfstaff.com.au/wp-content/uploads/2017/12/placeholder-image-300x207.png",
    };
    this.handleBack = this.handleBack.bind(this);
  }

  //form validate field
  validate = () => {
    let titleValidate = "";
    let descValidate = "";

    if (!this.state.title) {
      titleValidate = " * title cannot be blank";
    }

    if (!this.state.description) {
      descValidate = " * description cannot be blank";
    }

    if (descValidate || titleValidate) {
      this.setState({
        descValidate: descValidate,
        titleValidate: titleValidate,
      });
      return false;
    }
    return true;
  };

  //Upload image
  onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();
      reader.onload = (e) => {
        this.setState({
          selectedFile: e.target.result,
        });
      };
      reader.readAsDataURL(event.target.files[0]);
      console.log(this.state.selectedFile);
    }
  };

  componentWillMount() {
    this.getDetail();
    let pv = queryString.parse(this.props.location.search);
    console.log(pv.isUpdate);
    this.setState({
      spcId: pv.id,
      isUpdate: pv.isUpdate,
    });
    this.getDetail(pv.id);

    pv.isUpdate
      ? this.setState({
          header: "Update Article",
          btnName: "Update",
        })
      : this.setState({
          header: "Add Article",
          btnName: "submit",
        });
  }

  // On Change Field
  handleOnChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  //Back button
  handleBack() {
    let path = `/`;
    this.props.history.push(path);
  }

  //handle submit
  handleOnSubmit = (event) => {
    const isValid = this.validate();

    if (this.state.isUpdate) {
      this.handleOnUpdate();
    } else {
      event.preventDefault();
      if (isValid) {
       // debugger;
        Axios.post(`http://110.74.194.124:15011/v1/api/articles`, {
          TITLE: this.state.title,
          DESCRIPTION: this.state.description,
          IMAGE: this.state.selectedFile,
        }).then((res) => {
          // debugger
          console.log(res.data);
          if (window.confirm(res.data.MESSAGE)) {
            this.props.history.push("/");
          }
        });
        console.log(this.state.title);
      }
    }
  };

  getDetail = (id) => {
    Axios.get("http://110.74.194.124:15011/v1/api/articles/" + id).then(
      (res) => {
        this.setState({
          data: res.data.DATA,
          title: res.data.DATA.TITLE,
          description: res.data.DATA.DESCRIPTION,
          selectedFile: res.data.DATA.IMAGE,
        });
        console.log(res);
      }
    );
  };

  //Update
  handleOnUpdate = () => {
    var body = {
      TITLE: this.state.title,
      DESCRIPTION: this.state.description,
      IMAGE: this.state.selectedFile,
    };
    const isValid = this.validate();
    if (isValid) {
      Axios.put(
        "http://110.74.194.124:15011/v1/api/articles/" + this.state.spcId,
        body
      ).then((res) => {
        console.log(res);
        if (window.confirm(res.data.MESSAGE)) {
          this.props.history.push("/");
        }
      });
    }
  };

  render() {
    return (
      <div className="container">
        <h1 style={{ marginTop: "20px" }}>{this.state.header}</h1>
        <div className="row">
          <div className="col-sm-8">
            <Form>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>
                  TITLE{" "}
                  <span style={{ fontSize: 16, color: "red" }}>
                    {this.state.titleValidate}
                  </span>{" "}
                </Form.Label>
                <Form.Label className="text-danger" id="msgErr"></Form.Label>
                <Form.Control
                  id="title"
                  value={this.state.title}
                  type="text"
                  placeholder="Enter Title"
                  name="title"
                  onChange={this.handleOnChange}
                />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>
                  DESCRIPTION{" "}
                  <span style={{ fontSize: 16, color: "red" }}>
                    {this.state.descValidate}
                  </span>{" "}
                </Form.Label>
                {/* <Form.Control
                  id="description"
                  type="text"
                  value={this.state.description}
                  placeholder="Enter Description"
                  name="description"
                  onChange={this.handleOnChange}
                /> */}
                <div className="input-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text" id="basic-addon">
                    <FontAwesomeIcon icon={faPen} />
                    </span>
                  </div>
                  <textarea
                    className="form-control"
                    name="description"
                    onChange={this.handleOnChange}
                    value={this.state.description}
                    id="description"
                    rows="5"
                  ></textarea>
                </div>
              </Form.Group>
              <Button variant="primary" onClick={this.handleOnSubmit}>
                {this.state.btnName}
              </Button>
              &nbsp;
              <Button
                variant="secondary"
                type="submit"
                onClick={this.handleBack}
              >
                Cancel
              </Button>
            </Form>
          </div>
          <div className="col-sm-4">
            <img
              width="200"
              id="target"
              src={this.state.selectedFile}
              alt="pict"
            />
            <div>
              <input
                type="file"
                onChange={this.onImageChange}
                className="filetype"
                id="group_image"
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
