import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Menu from './Components/Menu/Menu'
import ViewArticleAll from './Components/ViewArticleAll/ViewArticleAll'
import AddArticle from './Components/AddArticle/AddArticle'
import ViewDetail from './Components/ViewDetail/ViewDetail'
import 'bootstrap/dist/css/bootstrap.min.css';

export default class App extends Component {       
    render() {
        return (
            <div>
                <Router>
                    <Menu />
                    <Switch>
                        <Route path="/" exact component={ViewArticleAll}/>
                        <Route path="/add" render={(props)=> <AddArticle {...props}/>} />
                        <Route path="/view/:id" component={ViewDetail}/>
                    </Switch>
                </Router>
            </div>
        )
    }
}
